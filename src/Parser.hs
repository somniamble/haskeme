module Parser
    ( parseExpression
    , readExpression
    , readManyExpressions
    , parseInterTokenSpace -- for testing purposes
    ) where

import LispError
import Control.Monad.Except (throwError)
import Text.ParserCombinators.Parsec hiding (spaces, try)
import Text.Parsec (try, parserFail)
import Control.Monad
import Control.Applicative (liftA, liftA2)
import Data.List (zipWith)
import LispVal
import Numeric (readOct, readHex)
import Data.Either

{-| some notes, for my benefit
 - `data ParsecT s u m a` (also known as `ParsecT s u (m :: * -> *) a`)
 "ParserT monad transformer (?) and Parser type

 "ParsecT s u m a is a parser with 
  - stream type `s` (?)
  - user state type `u` (?)
  - underlying monad `m`
  - and return type `a`"
  
 " Parsec is strict in the user state. If this is undesirable, simply use a data type like data Box a = Box a and the state type Box YourStateType to add a level of indirection."
  
 - `type Parsec s u = ParsecT s u Identity` (also known as Text.Parsec.Prim.ParsecT s u Identity :: * -> * ))
 - so, the monad transformer is Identity, which means Parsec type is a non-transformer version of ParsecT
 - - I guess the Identity monad accounts for `m` and `a`? since the return type is simply whatever is passed in...
 - - This is an assumption that I would like to confirm.
 - stream type `s`
 - user state type `u`
 
 - type Parser = Parsec String () :: * -> *
 

--- Identity Monad ---
-- "The Identity monad is a monad that does not embody any computational strategy. It simply applies the bound function to its input without any modification. Computationally, there is no reason to use the Identity monad instead of the much simpler act of simply applying functions to their arguments. The purpose of the Identity monad is its fundamental role in the theory of monad transformers. Any monad transformer applied to the Identity monad yields a non-transformer version of that monad."
-- so, the usage of the Identity monad with Parsec parsers is to make a non-transformer version of the parsec parser monad transformer

--- Stream type ---
-- `class Monad m => Stream s m t | s -> t` where
-- `uncons :: s -> m (Maybe (t, s))`
 - An instance of Stream has 
 - stream type `s`, 
 - underlying monad `m`,
 - and token type `t` determined by the stream

-- :t parse
-- `Stream s Identity t => Parsec s () a -> SourceName -> s -> Either ParseError a`
-- is `t` token type? wha
-- :t parse letter
-- Stream s Identity Char => SourceName -> s -> Either ParseError Char 
-- is it because of the Identity type that `t` (token type?) is equivalent to return type `a`?
--
-- ok, all of this in mind, let's proceed.
-}


-- the token type of the stream for the Parser `spaces` is `Char`
-- the return type of the Parser `spaces` is `()`
spaces :: Parser () -- `Unit` return type
spaces = skipMany1 space -- `space` is a Parser Char, like `symbol` above
-- so, then the token type and the return type need not be the same

delimiter :: Parser Char
delimiter = space <|> oneOf "()\";"

delimiterOrEof :: Parser Char
delimiterOrEof = delimiter <|> (' ' <$ eof)

-- All of the non-alphabetical and non-digit ascii characters representable without an escape code (like space or whatever)
nonAlphaNumeric :: Parser Char
nonAlphaNumeric = oneOf "!\"#$%&()*+,-./:;<=>?1[\\]^_`{|}]'"

characterName :: Parser String
characterName = choice ((try . string) <$> strings)
  where strings  = ["altmode"
                   , "backnext"
                   , "backspace"
                   , "call"
                   , "linefeed"
                   , "page"
                   , "return"
                   , "rubout"
                   , "space"
                   , "tab"
                   ]

characterNameToChar :: String -> Char
characterNameToChar "altmode"   = '\ESC'
characterNameToChar "backnext"  = '\US'
characterNameToChar "backspace" = '\BS'
characterNameToChar "call"      = '\SUB'
characterNameToChar "linefeed"  = '\LF'
characterNameToChar "page"      = '\FF'
characterNameToChar "return"    = '\CR'
characterNameToChar "rubout"    = '\DEL'
characterNameToChar "space"     = ' '
characterNameToChar "tab"       = '\HT'
characterNameToChar [c]         = c
characterNameToChar _           = undefined -- TODO: make this a proper error.

-- [DONE] [Exercies 5.] Add a `Character` constructor to LispVal and create a parser for character literals as described in R5RS
parseCharacter :: Parser LispVal
parseCharacter = let
  charName  = (characterNameToChar <$> characterName) <* lookAhead delimiterOrEof
  in do
    value <- char '\\' >> ((try charName) <|> (anyChar <* lookAhead delimiterOrEof))
    return $ Character value

parseOctal :: Parser LispVal
parseOctal = char 'o' >> (Number . fst . head . readOct) <$> (many1 $ oneOf "01234567")

parseHex :: Parser LispVal
parseHex = char 'x' >> (Number . fst . head . readHex) <$> (many1 $ (oneOf "ABCDEFabcdef" <|> digit))

-- [TODO]  finish implementing the Vector type
parseVector :: Parser LispVal
parseVector = between (char '(' >> optional spaces) (char ')') $ Vector <$> (sepEndBy parseExpression spaces)

-- [TODO] define parseBinary and add it to parseSharpPrefixed
parseBinary :: Parser LispVal
parseBinary = undefined

parseBool :: Parser LispVal
parseBool = (Bool True <$ char 't') <|> (Bool False <$ char 'f')

parseSharpPrefixed :: Parser LispVal
parseSharpPrefixed = char '#' >> choice [parseBool, parseHex, parseOctal, parseCharacter, parseVector]
 


-- [DONE] [Exercise 2.] change parseString so that \" gives a literal quote character instead of terminating the string
-- [DONE] [Exercise 3.] modify parseString to be fully R5RS compliant (support all escape chars)
parseString :: Parser LispVal -- a parser with return type LispVal
parseString = String <$> between (char '"') (char '"') (many $ (parseEscaped <|> notQuote)) where
  notQuote     = noneOf "\""
  parseEscaped = char '\\' >> (choice $ zipWith (\c e -> char c >> return e) code escaped)
  escaped      = ['\r', '\n', '\t', '\"', '\\']
  code         =  ['r',  'n',  't',  '"', '\\']
-- parseString = do
--   char '"' -- get a '"' and ignore it
--   x <- many $ noneOf "\"" -- noneOf "\"" is a parser with return type Char (Parser Char), many $ noneOf "\"" is a Parser [Char] which gets any number of non-quote characters
--   char '"'
--   return $ String x -- return a String lispval

parsePeculiar :: Parser LispVal
parsePeculiar = Atom <$> (string "..." <|> pure <$> (oneOf "+-" <* lookAhead delimiterOrEof))

parseIdentifier :: Parser LispVal
parseIdentifier = let 
    specialInitial     = oneOf "!$%&*/:<>?^_~|"
    specialSubsequent  = oneOf "+-.@" 
    initial            = letter <|> specialInitial
    subsequent         = many $ letter <|> specialInitial <|> digit <|> specialSubsequent in
  Atom <$> ((:) <$> initial <*> subsequent)

--seqParsers :: [Parser String] -> Parser String
--seqParsers = concat <$> sequence

-- [DONE] rewrite in applicative style
-- [DONE] [Exercise 6.] add a float constructor to LispVal, and add support for R5RS syntax for decimals
parseFloat :: Parser LispVal
parseFloat = liftM (Float . read) $ concat <$> sequence [(option "" $ string "-")   -- get a negative sign or supply empty string
                                                       , (option "0" $ many1 digit) -- get 1 or more digits or supply 0
                                                       , (string ".")               -- get a '.'
                                                       , (many1 digit)]             -- get 1 or more digits
--parseFloat = let conc = liftA2 (++) in
                 --(Float . read) <$> (((option "" $ string "-") `conc` (option "0" $ many1 digit)) `conc` (string ".") `conc` (many1 digit)) where
--parseFloat = let conc = liftA2 (++) in
                 --liftM (Float . read) $ foldl conc (pure "") [(option "" $ string "-") , (option "0" $ many1 digit) , (string ".") , (many1 digit)]
--parseFloat = do
--  sign <- option "" $ ((:[]) <$> char '-')
--  whole <- option "0" $ many1 digit
--  char '.'
--  decimal <- many1 digit
--  return $ (Float . (read :: String -> Float)) $ sign ++ whole ++ "." ++ decimal where
--    conc = liftA2 (++)
-- [DONE] replace negativeApplier with `option` parse, and write in applicative style
-- [DONE] Exercise 1. rewrite parseNumber without liftM, using 1) do-notation 2) explicit sequencing with `>>=`
-- [in-progress] [Exercise 7]. Add data types and parsers to support the full numeric tower of Scheme numeric types
parseNumber :: Parser LispVal
parseNumber = liftA2 intOrFloat integerPart decimalPart where-- the liftM turns (Number . read) into 
  integerPart = liftA2 (++) (option "" $ string "-") (many1 digit) :: Parser String
  decimalPart = optionMaybe (liftA2 (:) (char '.') $ many1 digit)  :: Parser (Maybe String)
  intOrFloat intPart (Just decPart) = Float . read $ intPart ++ decPart
  intOrFloat intPart Nothing        = Number $ read intPart
--parseFloat = liftM (Float . read) $ concat <$> sequence [(option "" $ string "-")   -- get a negative sign or supply empty string
--                                                       , (option "0" $ many1 digit) -- get 1 or more digits or supply 0
--                                                       , (string ".")               -- get a '.'
--                                                       , (many1 digit)]             -- get 1 or more digits
-- parseNumber = return . (Number . read) =<< many1 digit -- compose `return` onto `Number . read` and bind `many1 digit` to it, to give it all Parser LispVal type
-- parseNumber = do
--   digits <- many1 digit -- extract the string of digits out of the stream
--   return . Number $ read digits -- compose `Number` with `return` to give it the right type, and feed it the Integer produced by `read digits`

-- [DONE] left-factor the grammar to parse a list or dotted list
parseList :: Parser LispVal
parseList = between (char '(' >> optional spaces) (char ')') $ join (makeList <$> parseListHead <*> parseListTail) where
  parseListHead        = sepEndBy parseExpression spaces                                        -- left-factored common prefix of list and dotted list
  parseListTail        = optionMaybe (char '.' >> spaces >> parseExpression <* optional spaces) -- matches ". <expr>" or yields Nothing
  makeList [] (Just _) = parserFail "Invalid use of '.'" :: Parser LispVal
  makeList xs (Just x) = return $ DottedList xs x
  makeList xs Nothing  = return $ List xs


-- [DONE] implement pointfree
parseQuoted :: Parser LispVal
parseQuoted = List . (Atom "quote" :) . pure <$> (char '\'' >> parseExpression)

parseQuasiQuoted :: Parser LispVal
parseQuasiQuoted = undefined

parseComment :: Parser ()
parseComment = char ';' >> manyTill anyChar newline >> return ()

parseInterTokenSpace :: Parser ()
parseInterTokenSpace = (spaces >> skipMany parseComment)

parseExpression :: Parser LispVal
parseExpression = (try parsePeculiar) -- "..." or "+" or "-"
                <|> (parseIdentifier <* lookAhead delimiterOrEof)
                <|> (parseNumber <* lookAhead delimiterOrEof)
                <|> parseSharpPrefixed 
                <|> parseString
                <|> parseQuoted
                <|> parseList

-- semantics of `(>>)` for Parser monad is, basically
-- attempt to match the first parser,
-- then attempt to match the second parser with the remaining input
-- fail if either fails
-- readExpression input = case parse (spaces >> symbol) "lisp" input of
--      Left err -> "No match: " ++ show err
--      Right val -> "Found Value: " ++ [val]
-- readExpression :: String -> ThrowsError LispVal
-- readExpression input = case parse parseExpression "lisp" input of
--      Left err -> throwError $ Parser err -- `Left <parser error>`
--      Right val -> return val             -- `Right <value>`

-- reads a single expression
readExpression :: String -> ThrowsError LispVal
readExpression = readOrThrow parseExpression

-- reads a collection of expressions separated by whitespace and comments
readManyExpressions :: String -> ThrowsError [LispVal]
readManyExpressions = readOrThrow (skipMany parseInterTokenSpace >> (sepEndBy parseExpression parseInterTokenSpace))

-- takes a parser and factors it
readOrThrow :: Parser a -> String -> ThrowsError a 
readOrThrow parser = either (throwError . Parser) (return) . (parse parser "haskeme")
--readOrThrow parser input = case parse parser "haskeme" input of
--                             Left err  -> throwError $ Parser err
--                             Right val -> return val
