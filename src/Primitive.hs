{-# LANGUAGE ExistentialQuantification #-}

module Primitive where

import Parser 
import LispVal
import LispError
import Control.Applicative (liftA2)
import Control.Monad (mapM, liftM, (<=<))
import Data.List (zipWith)
import Data.IORef
import System.IO
import Control.Monad.Except

ioPrimitives :: [(String, [LispVal] -> IOThrowsError LispVal)]
ioPrimitives = [ ("open-input-file"  , makePort ReadMode)
               , ("open-output-file" , makePort WriteMode)
               , ("close-input-port" , closePort)
               , ("close-output-port", closePort)
               , ("read"             , readProc)
               , ("write"            , writeProc)
               , ("read-contents"    , readContents)
               , ("read-all"         , readAll) ]


-- a mapping between an identifier name, and a function taking a list of arguments which may throw an error
primitives :: [(String, [LispVal] -> ThrowsError LispVal)]
primitives = [ ("+",         numericBinop (+))
             , ("-",         numericBinop (-))
             , ("*",         numericBinop (*))
             , ("/",         numericBinop (div))
             , ("mod",       numericBinop (mod))
             , ("quotient",  numericBinop (quot))
             , ("remainder", numericBinop (rem))
             , ("=",         numericBoolBinop (==))
             , ("<",         numericBoolBinop (<))
             , (">",         numericBoolBinop (>))
             , ("/=",        numericBoolBinop (/=))
             , (">=",        numericBoolBinop (>=))
             , ("<=",        numericBoolBinop (<=))
             , ("&&",        booleanBoolBinop (&&))
             , ("||",        booleanBoolBinop (||))
             , ("string=?",  stringBoolBinop (==))
             , ("string<?",  stringBoolBinop (<))
             , ("string>?",  stringBoolBinop (>))
             , ("string<=?", stringBoolBinop (<=))
             , ("string>=?", stringBoolBinop (>=))
             , ("cons", cons)
             , ("car", car)
             , ("cdr", cdr)
             , ("eqv?", eqv)
             , ("eq?", eqv)
             , ("equal?", equal)
             , ("string?", isString)
             , ("make-string", makeString)
             , ("string-length", stringLength)
             , ("string-ref", stringRef)
             , ("string-set!", stringSet)
             , ("char?", isChar)
             , ("char=?", charEquals)
             , ("char<?", charLessThan)
             , ("char>?", charGreaterThan)
             , ("char<=?", charLessThanEquals)
             , ("char>=?", charGreaterThanEquals)
             , ("char->integer", charToInt)
             , ("integer->char", intToChar)
             ]



  {-  COMMENTED OUT FOR CLARITY
numericBinop :: (Integer -> Integer -> Integer) -> [LispVal] -> ThrowsError LispVal
numericBinop operator []           = throwError $ NumArgs 2 []                            -- handle the case where no arguments are applied
-- these don't work.
--numericBinop (-) [x]               = Number . negate <$> (unpackNum x)                    -- handle the case where (-) results in negation
--numericBinop (/) [x]               = Float . recip . fromInteger <$> (unpackNum x)        -- handle the case where (/) results in multiplicative inversion
numericBinop operator single@([_]) = throwError $ NumArgs 2 single                        -- handle the case where more than 1 argument is expected
-- mapM :: (a -> m b) -> [a] -> m [b]
-- takes a monadic function, maps it over a list of a, and applys `sequence` to give a list in the monad
numericBinop operator params       = (Number . foldl1 operator) <$> mapM unpackNum params -- handle the case where the correct number of arguments are supplied
--numericBinop operator params       = return . Number $ foldl1 operator $ map unpackNum params
 -}
numericBinop :: (Integer -> Integer -> Integer) -> [LispVal] -> ThrowsError LispVal
numericBinop operator []           = throwError $ NumArgs 2 []                            -- handle the case where no arguments are applied
numericBinop operator single@([_]) = throwError $ NumArgs 2 single                        -- handle the case where more than 1 argument is expected
numericBinop operator params       = (Number . foldl1 operator) <$> mapM unpackNum params -- handle the case where the correct number of arguments are supplied

-- [TODO] implement handling for Float values
unpackNum :: LispVal -> ThrowsError Integer
unpackNum (Number n) = return n                                                    -- handle the case where an integer is passed in
unpackNum (String n) = case (reads n :: [(Integer, String)]) of
                         []       -> throwError $ TypeMismatch "number" $ String n -- handle the case where an integer cannot be parsed from the string
                         [(n, _)] -> return n                                      -- handle the case where an integer is parsed from the string successfully
-- note, this case will handle an arbitrarily nested single list, e.g. (1) or ((((((1))))))
unpackNum (List [n]) = unpackNum n                                                 -- remove the single element from the list and pass it to unpackNum
unpackNum notNumber = throwError $ TypeMismatch "number" notNumber                 -- handle the case where there is no integer parsed from the string

-- boolBinop :: (LispVal -> ThrowsError a) -> (a -> a -> Bool) -> [LispVal] -> ThrowsError LispVal
-- boolBinop

-- [TODO] implement Character unpack

unpackBool :: LispVal -> ThrowsError Bool
unpackBool (Bool b) = return b
unpackBool notBool  = throwError $ TypeMismatch "boolean" notBool

unpackString :: LispVal -> ThrowsError String
unpackString (String s)    = return s
unpackString (Number s)    = return $ show s
unpackString (Float s)     = return $ show s
unpackString (Bool s)      = return $ show s
unpackString (Character s) = return $ show s
unpackString notString     = throwError $ TypeMismatch "string" notString

-- takes a value which unpacks a lispval, a binary operator resulting in a boolean, a list of arguments, and returns a LispVal in the error monad
boolBinop :: (LispVal -> ThrowsError a) -> (a -> a -> Bool) -> [LispVal] -> ThrowsError LispVal
boolBinop unpacker operator [x, y] = Bool <$> liftA2 operator (unpacker x) (unpacker y)
boolBinop _ _ invalidArgs          = throwError $ NumArgs 2 invalidArgs

numericBoolBinop :: (Integer -> Integer -> Bool) -> [LispVal] -> ThrowsError LispVal
numericBoolBinop = boolBinop unpackNum

booleanBoolBinop :: (Bool -> Bool -> Bool) -> [LispVal] -> ThrowsError LispVal
booleanBoolBinop = boolBinop unpackBool

stringBoolBinop :: (String -> String -> Bool) -> [LispVal] -> ThrowsError LispVal
stringBoolBinop = boolBinop unpackString

-- list operations
-- car 
-- 1. (car '(a b c)) = a
-- 2. (car '(a)) = a
-- 3. (car '(a b . c)) = a
-- 4. (car 'a) = error – not a list
-- 5. (car 'a 'b) = error – car only takes one argument
car :: [LispVal] -> ThrowsError LispVal
car [List (x:xs)]          = return x -- catches the case when the argument list is a single list, cases 1 & 2 of above
car [DottedList (x:xs) _ ] = return x -- catches the case when the argument list is a single dotted-list, case 3 of above
car [invalidArgument]      = throwError $ TypeMismatch "pair" invalidArgument -- catches the case when the argument list is a single non-list argument (error case), case 4 of above
car badArgumentList        = throwError $ NumArgs 1 badArgumentList           -- catches the case when the argument list is anything else (error case), case 5 of above, and more not listed

-- cdr
-- 1. (cdr '(a b c)) = (b c)
-- 2. (cdr '(a b)) = (b)
-- 3. (cdr '(a)) = NIL
-- 4. (cdr '(a . b)) = b
-- 5. (cdr '(a b . c)) = (b . c)
-- 6. (cdr 'a) = error – not a list
-- 7. (cdr 'a 'b) = error – too many arguments

cdr :: [LispVal] -> ThrowsError LispVal
cdr [List (_:xs)]       = return $ List xs          -- returns a List, case  1, 2, 3 of above
cdr [DottedList [_] x]  = return x                  -- returns the value in the last (dotted) position of the list, as itself, case 4 of above
cdr [DottedList xs x]   = return $ DottedList xs x  -- returns the tail of the list as well as the dotted item, case 5 of above
cdr [invalidArgument]   = throwError $ TypeMismatch "pair" invalidArgument
cdr invalidArgumentList = throwError $ NumArgs 1 invalidArgumentList

-- cons
-- 1. (cons 1 '())      = (1)
-- 2. (cons 1 '(2))     = (1 2)
-- 3. (cons 1 '(2 . 3)) = (1 2 . 3)
-- 4. (cons 1 2)        = (1 . 2)
-- 5. (cons 1 2 3)      = error, too many arguments
-- 6. (cons 1)          = error, too few arguments
cons :: [LispVal] -> ThrowsError LispVal
cons [x, List []]            = return $ List [x]
cons [x, List xs]            = return $ List $ x : xs
cons [x, DottedList xs last] = return $ DottedList (x:xs) last
cons [x1, x2]                = return $ DottedList [x1] x2
cons invalidArgumentList     = throwError $ NumArgs 2 invalidArgumentList

-- takes two lispvals and returns a Boolean LispVal
eqv :: [LispVal] -> ThrowsError LispVal
eqv [(Bool arg1), (Bool arg2)]             = return $ Bool $ arg1 == arg2
eqv [(Number arg1), (Number arg2)]         = return $ Bool $ arg1 == arg2
eqv [(String arg1), (String arg2)]         = return $ Bool $ arg1 == arg2
eqv [(Character arg1), (Character arg2)]   = return $ Bool $ arg1 == arg2
eqv [(Float arg1), (Float arg2)]           = return $ Bool $ arg1 == arg2
eqv [(Atom arg1), (Atom arg2)]             = return $ Bool $ arg1 == arg2
eqv [(List xs), (List ys)]                 = let eqvZip p q = eqv [p, q] in -- makes 2 args into a list and passes to eqv
                                                 Bool <$> ((length xs == length ys) &&) <$> -- check if |xs| == |ys|
                                                          (all (== Bool True) <$> (sequence $ zipWith eqvZip xs ys))
eqv [(DottedList xs x), (DottedList ys y)] = eqv [List (x:xs), List (y:ys)]
eqv [_, _]                                 = return $ Bool False
eqv badArgs                                = throwError $ NumArgs 2 badArgs

-- squashes the unpacker functions into a single data type
data Unpacker = forall a. Eq a => AnyUnpacker (LispVal -> ThrowsError a)

-- write it first THEN refactor.
-- I am in the habit of forgetting this
unpackEquals :: LispVal -> LispVal -> Unpacker -> ThrowsError Bool
unpackEquals arg1 arg2 (AnyUnpacker unpacker) = catchError equality' $ const $ return False where
    equality' = do
      p <- unpacker arg1
      q <- unpacker arg2
      return $ p == q

equal :: [LispVal] -> ThrowsError LispVal
equal [arg1, arg2] = do
  primitiveEquals <- liftM or $ mapM (unpackEquals arg1 arg2) unpackers
  eqvEquals       <- unpackBool =<< eqv [arg1, arg2]
  return $ Bool $ (primitiveEquals || eqvEquals) where
    unpackers = [AnyUnpacker unpackNum, AnyUnpacker unpackString, AnyUnpacker unpackBool]
equal badArgs      = throwError $ NumArgs 2 badArgs

--[TODO] implement string functions

isString :: [LispVal] -> ThrowsError LispVal
isString = undefined

makeString :: [LispVal] -> ThrowsError LispVal
makeString = undefined

stringLength :: [LispVal] -> ThrowsError LispVal
stringLength = undefined

stringRef :: [LispVal] -> ThrowsError LispVal
stringRef = undefined

stringSet :: [LispVal] -> ThrowsError LispVal
stringSet = undefined

-- [TODO] implement character procedures 


-- procedure:  (char? obj) 
isChar :: [LispVal] -> ThrowsError LispVal
isChar = undefined
-- Returns #t if obj is a character, otherwise returns #f.
-- 
-- procedure:  (char=? char1 char2) 
charEquals :: [LispVal] -> ThrowsError LispVal
charEquals = undefined

-- procedure:  (char<? char1 char2) 
charLessThan :: [LispVal] -> ThrowsError LispVal
charLessThan = undefined

-- procedure:  (char>? char1 char2) 
charGreaterThan :: [LispVal] -> ThrowsError LispVal
charGreaterThan = undefined

-- procedure:  (char<=? char1 char2) 
charLessThanEquals :: [LispVal] -> ThrowsError LispVal
charLessThanEquals = undefined

-- procedure:  (char>=? char1 char2) 
charGreaterThanEquals :: [LispVal] -> ThrowsError LispVal
charGreaterThanEquals = undefined

-- procedure:  (char->integer char) 
charToInt :: [LispVal] -> ThrowsError LispVal
charToInt = undefined

-- procedure:  (integer->char n) 
intToChar :: [LispVal] -> ThrowsError LispVal
intToChar = undefined

  {- IO PRIMITIVES -}

-- opens a file handle, returning an if it is not given a file handle
makePort :: IOMode -> [LispVal] -> IOThrowsError LispVal
makePort mode [String filePath] = liftM Port $ liftIO $ openFile filePath mode

-- closes a file handle, or silently fails if it is not given a file handle
closePort :: [LispVal] -> IOThrowsError LispVal
closePort [Port port] = liftIO $ hClose port >> (return $ Bool True)
closePort _           = return $ Bool False

-- reads a string in from a file handle (stdin by default)
readProc :: [LispVal] -> IOThrowsError LispVal
readProc []          = readProc [Port stdin]
readProc [Port port] = liftM String $ liftIO $ hGetLine port

-- prints a value out to a file handle (stdout by default)
writeProc :: [LispVal] -> IOThrowsError LispVal
writeProc [obj]            = writeProc [obj, Port stdout]
writeProc [obj, Port port] = liftIO $ hPrint port obj >> (return $ Bool True) 

-- reads a file into a single string
readContents :: [LispVal] -> IOThrowsError LispVal
readContents [String filePath] = liftM String $ liftIO $ readFile filePath

-- reads a list of expressions from a file
load :: String -> IOThrowsError [LispVal]
load filePath = (liftIO $ readFile filePath) >>= liftThrows . readManyExpressions

-- converts a list of expressions read from a file into a Lisp list of expressions
readAll :: [LispVal] -> IOThrowsError LispVal
readAll [String filePath] = liftM List $ load filePath
