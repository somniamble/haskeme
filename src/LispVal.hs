module LispVal where
--module LispVal (LispVal (..)) where

import Data.IORef -- IORef
import System.IO  -- Handle
import Control.Monad.Except
import Text.Parsec

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Character Char
             | Number Integer
             | Float Float 
             | String String
             | Bool Bool
             | Vector [LispVal] -- [TODO] finish implementing the vector type
             | PrimitiveFunc ([LispVal] -> Either LispError LispVal)
             | Func LispFunc
             | IOFunc ([LispVal] -> IOThrowsError LispVal)
             | Port Handle

data LispFunc = LispFunc { params  :: [String]
                         , vararg  :: (Maybe String)
                         , body    :: [LispVal]
                         , closure :: Env }

data LispError = NumArgs Integer [LispVal]      -- NumArgs <expected number of arguments> <list of arguments>
              | TypeMismatch String LispVal     -- TypeMistmatch <expected type> <actual value>
              | Parser ParseError               -- Parser <error message from parser>
              | BadSpecialForm String LispVal   -- BadSpecialForm 
              | NotFunction String String       -- NotFunction <error message> <identifier>
              | UnboundVar String String
              | Default String

type Env = IORef [(String, IORef LispVal)]
type ThrowsError = Either LispError
type IOThrowsError = ExceptT LispError IO

characterToName :: Char -> String
characterToName '\ESC' = "altmode"
characterToName '\US'  = "backnext"
characterToName '\BS'  = "backspace"
characterToName '\SUB' = "call"
characterToName '\LF'  = "linefeed"
characterToName '\FF'  = "page"
characterToName '\CR'  = "return"
characterToName '\DEL' = "rubout"
characterToName ' '    = "space"
characterToName '\HT'  = "tab"
characterToName c      = [c]

instance Show LispVal where
  show (Atom name)                 = name
  show (String contents)           = "\"" ++ contents ++ "\""
  show (Number num)                = show num
  show (Float num)                 = show num
  show (Bool True)                 = "#t"
  show (Bool False)                = "#f"
  show (List [Atom "quote", val])  = "'" ++ show val
  show (List list)                 = "("++ (unwords $ show <$> list) ++ ")"
  show (DottedList xs x)           = "(" ++ (unwords $ show <$> xs) ++ " . " ++ show x ++ ")"
  show (Character c)               = "#\\" ++ characterToName c
  show (Vector v)                  = undefined
  show (PrimitiveFunc _)           = "<primitive>"
  show (Port _)                    = "<IO port>"
  show (IOFunc _)                  = "<IO primitive>"
--  show (Func LispFunc {params = args, vararg = varargs, body = body, closure = env}) = -- unnecessarily cumbersome
  show (Func (LispFunc args varargs _ _)) =
    "(lambda (" 
      ++ unwords (show <$> args) 
      ++ (case varargs of Nothing  -> "" ; Just arg -> " . " ++ arg) 
      ++ ") ...)"

instance Eq LispVal where
  (Atom name1) == (Atom name2)                   = name1 == name2
  (String s1) == (String s2)                     = s1 == s2
  (Number n1) == (Number n2)                     = n1 == n2
  (Float f1) == (Float f2)                       = f1 == f2
  (Bool b1) == (Bool b2)                         = b1 == b2
  (Character c1) == (Character c2)               = c1 == c2
  (List lst1) == (List lst2)                     = lst1 == lst2
  (DottedList lst1 end1) == (DottedList lst2 end2) = lst1 == lst2 && end1 == end2
  (List lst1) == (DottedList lst2 tl2)           = (List lst1) == (List $ lst2 ++ [tl2])
  dotted@(DottedList _ _) == list@(List _)       = list == dotted
  (Vector v1) == (Vector v2)                     = undefined
  _ == _                                         = False

instance Show LispError where
  show (UnboundVar message varName)  = message ++ ": " ++ varName
  show (BadSpecialForm message form) = message ++ ": " ++ show form
  show (NotFunction message func)    = message ++ ": " ++ show func
  show (NumArgs expected found)      = "Expected " ++ show expected ++ " args, found values " ++ (unwords $ show <$> found)
  show (TypeMismatch expected found) = "Invalid type: expected " ++ expected ++ ", found " ++ show found
  show (Parser parseError)           = "Parse error at " ++ show parseError


-- isAtom :: LispVal -> Bool
-- isAtom (Atom _) = True
-- isAtom _        = False
-- 
-- isString :: LispVal -> Bool
-- isString (String _) = True
-- isString _          = False
-- 
-- isNumber :: LispVal -> Bool
-- isNumber (Number _) = True
-- isNumber _          = False
-- 
-- isFloat :: LispVal -> Bool
-- isFloat (Float _) = True
-- isFloat _         = False
-- 
-- isBool :: LispVal -> Bool
-- isBool (Bool _) = True
-- isBool _        = False
-- 
-- isList :: LispVal -> Bool
-- isList (List _) = True
-- isList _        = False
-- 
-- isCharacter :: LispVal -> Bool
-- isCharacter (Character _) = True
-- isCharacter _             = False
-- 
-- isDottedList :: LispVal -> Bool
-- isDottedList (DottedList _ _) = True
-- isDottedList _                = False
-- 
-- isVector :: LispVal -> Bool
-- isVector (Vector _) = True
-- isVector _          = False

  {-|
  This is an example of an algebraic data type: it 
  defines a set of possible values that a variable of type LispVal can hold. 
  Each alternative (called a constructor and separated by |) contains a tag for the constructor,
    along with the type of data that the constructor can hold. 
  In this example, a LispVal can be:

  1. An Atom, which stores a String naming the atom
  2. A List, which stores a list of other LispVals (Haskell lists are denoted by brackets); also called a proper list
  3. A DottedList, representing the Scheme form (a b . c); also called an improper list. 
     This stores a list of all elements but the last, and then stores the last element as another field
  4. A Number, containing a Haskell Integer
  5. A String, containing a Haskell String
  6. A Bool, containing a Haskell boolean value
 -}
