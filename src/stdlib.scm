(define (not x) (if x #f #t))

(define (null? lst) (eqv? lst '()))

(define (list . arguments) arguments)

(define (id x) x)

(define (flip f)
  (lambda (x y) (f y x)))

(define (compose f g) 
  (lambda (x) (f (g x))))

(define (curry f arg1)
  (lambda (arg2) (apply f (cons arg1 (list arg2)))))

(define zero? (curry eqv? 0))

(define negative? (curry > 0))

(define positive? (curry < 0))

(define even? (compose zero? 
                       (curry (flip mod) 2)))

(define odd? (compose not 
                      (compose zero? 
                               (curry (flip mod) 2))))

(define (foldr infix end lst)
  (if (null? lst)
    end
    (infix (car lst) (foldr infix end (cdr lst)))))

(define (foldl infix accum lst)
  (if (null? lst)
    accum
    (foldl infix (infix accum (car lst) ) (cdr lst))))

(define fold foldl)

(define reduce foldr)

(define (unfold func init pred)
  (if (pred init)
    '()
    (cons init (unfold func (func init) pred))))

(define (range init end step)
  (unfold (curry + step) init (curry < end)))

(define (sum . lst) (fold + 0 lst))
(define (product . lst) (fold * 1 lst))
(define (and . lst) (fold && #t lst))
(define (or . lst) (fold || #f lst))

(define (factorial n) 
  (if (zero? n)
    1
    (apply product (range 1 n 1))))

(define (map f lst)
  (reduce (lambda (x rest) (cons (f x) rest)) 
          '() lst))

(define (filter pred lst)
  (reduce (lambda (x rest) (if (pred x) (cons x rest) rest))
          '() lst))

(define (max first . rest) 
  (fold 
    (lambda (old new) (if (> new old) new old)) first rest))

(define (min first . rest) 
  (fold 
    (lambda (old new) (if (< new old) new old)) first rest))

(define (length lst)
  (fold (lambda (x y) (+ x 1)) 0 lst))
