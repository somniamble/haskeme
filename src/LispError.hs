module LispError ( LispError (..)
                 , ThrowsError
                 , IOThrowsError
                 , trapError
                 , extractValue
                 , throwError -- passed on from Control.Monad.Except
                 , catchError -- passed on form Control.Monad.Except
                 , liftThrows
                 , runIOThrows
  ) where
import Control.Monad.Except
import LispVal
import Text.Parsec


-- works both ways
-- I don't really know what the 'e' type is here
trapError :: (MonadError e m, Show e) => m String -> m String
trapError = flip catchError $ return . show
-- trapError action = catchError action (return . show)

-- strips the value out of a ThrowsError
extractValue :: ThrowsError a -> a
extractValue (Right val) = val

trapValue :: ThrowsError String -> String
trapValue action = extractValue $ trapError action

--liftThrows :: Either LispError a -> ExceptT (Either LispError a) IO a -- this does not work
liftThrows :: ThrowsError a -> IOThrowsError a
liftThrows (Left err)  = throwError err
liftThrows (Right val) = return val

-- runExceptT             ::                      ExceptT e m a -> m (Either e a)
-- runExceptT . trapError :: (Monad m, Show e) => ExceptT e m String -> m (Either e String)
-- so, it runs the action, and returns and `Either LispError String`, in the IO monad
runIOThrows :: IOThrowsError String -> IO String
runIOThrows action = (runExceptT . trapError $ action) >>= return . extractValue
--runIOThrows action = runExceptT (trapError action) >>= return . extractValue
--
-- :i ExceptT
--   newtype ExceptT e (m :: * -> *) a = ExceptT (m (Either e a))
--   (so, it is a type with an error type e, a monad type (of kind * -> *) m, and anytype a)
-- :k ExceptT
--   ExceptT :: * -> (* -> *) -> * -> *
--
-- :k IO
--   IO :: * -> *
--
-- 
