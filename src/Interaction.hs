module Interaction (runRepl, runOne) where

import Parser
import LispVal
import Evaluator
import LispError
import System.IO
import Control.Monad
import Data.IORef

-- evaluateToString :: String -> String
-- evaluateToString expr = extractValue $ trapError (liftM show $ readExpression expr >>= eval)
--evaluateToString = extractValue . trapError . (return . show . eval <=< readExpression)
-- 
-- liftThrows $ readExpression expr :: IOThrowsError LispVal
-- bind to (eval env) :: LispVal -> IOThrowsError LispVal
-- then `liftM show $ (liftThrows $ readExpression expr) >>= evalEnv :: IOThrowsError String` 
-- runIOThrows :: IOThrowsError String -> IO String
evalString :: Env -> String -> IO String
evalString env expr = runIOThrows $ liftM show $ (liftThrows $ readExpression expr) >>= eval env 

evalAndPrint :: Env -> String -> IO ()
evalAndPrint env expr = evalString env expr >>= putStrLn

flushStr :: String -> IO ()
flushStr str = putStr str >> hFlush stdout

readPrompt :: String -> IO String
readPrompt prompt = flushStr prompt >> getLine

until_ :: Monad m => (a -> Bool) -> m a -> (a -> m ()) -> m ()
until_ pred prompt action = do
  result <- prompt
  if pred result
     then return ()
     else action result >> until_ pred prompt action

runOne :: [String] -> IO ()
-- runOne expr = primitiveBindings >>= flip evalAndPrint expr
runOne args = do
  env <- primitiveBindings >>= flip bindVars [("args", arguments)]
  (runIOThrows $ liftM show $ eval env $ List [Atom "load", filePath]) >>= hPutStrLn stderr where
    filePath  = String $ head args
    arguments = List $ String <$> drop 1 args

runRepl :: IO ()
runRepl = primitiveBindings >>= until_ (== "quit") (readPrompt "Haskeme>>> ") . evalAndPrint
