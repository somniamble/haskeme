module Evaluator {-(
  eval
, evaluateToString 
)-}
where

import Parser 
import LispVal
import LispError
import Primitive
import Control.Applicative (liftA2)
import Control.Monad
import Control.Monad.Except (liftIO, ExceptT)
import Data.List (zipWith)
import Data.IORef


nullEnv :: IO Env
nullEnv = newIORef [] 

-- type Env = IORef [(String, IORef LispVal)]

applyProc :: [LispVal] -> IOThrowsError LispVal
applyProc [func, List args] = apply func args
applyProc (func : args)     = apply func args

primitiveBindings :: IO Env
primitiveBindings = nullEnv >>= (flip bindVars $ (ioPrims ++ prims)) where
  ioPrims = makeFunc IOFunc <$> (("apply", applyProc) : ioPrimitives)
  prims   = makeFunc PrimitiveFunc <$> primitives
  makeFunc constructor (identifier, function) = (identifier, constructor function)
  

quote :: LispVal
quote = Atom "quote"


eval :: Env -> LispVal -> IOThrowsError LispVal
eval env value@(String _)      = return value
eval env value@(Number _)      = return value
eval env value@(Bool _)        = return value
eval env value@(Character _)   = return value
eval env value@(Float _)       = return value
eval env (Atom id)             = getVar env id
eval env (List [Atom "if", predicate, consequent, alternate]) = do
                     result <- eval env predicate
                     case result of
                       Bool False -> eval env alternate
                       otherwise  -> eval env consequent
eval env (List (Atom "cond":clauses))     = undefined         -- [TODO] implement `cond`
eval env (List (Atom "case":key:clauses)) = undefined     -- [TODO] implement `case`
eval env (List [Atom "quote", value])     = return value
eval env (List [Atom "load", String filePath]) = 
  load filePath >>= liftM last . mapM (eval env) -- loads a list of values in from a file, and then evaluates them all and returns the last value
eval env (List [Atom "set!", Atom var, form]) = eval env form >>= setVar env var
eval env (List [Atom "define", Atom var, form]) = eval env form >>= defineVar env var
eval env (List (Atom "define" : List (Atom var : params) : body)) = -- (define (f x y) (+ x y))
  makeNormalFunction env params body >>= defineVar env var 
eval env (List (Atom "define" : (DottedList (Atom var : params) varargs) : body)) = -- (define (f x . y) (cons x y))
  makeVarArgFunction varargs env params body >>= defineVar env var
eval env (List (Atom "lambda" : (List params) : body)) = -- (lambda (x) (* x 2))
  makeNormalFunction env params body -- don't bind to environment
eval env (List (Atom "lambda" : (DottedList params varargs) : body)) = -- (lambda (x . y) (cons x y))
  makeVarArgFunction varargs env params body
eval env (List (Atom "lambda" : varargs@(Atom _) : body)) = -- (lambda x (print x)) ; x is a list
  makeVarArgFunction varargs env [] body
eval env (List (function : args)) = do
  function <- eval env function
  arguments <- sequence $ eval env <$> args
  apply function arguments
--eval env (List (Atom func : args))        = mapM (eval env) args >>= liftThrows . apply func -- so we have to use (>>=) to pass along the results of `mapM eval args :: ThrowsError [LispVal]` to `apply func :: [LispVal] -> ThrowsError LispVal`
eval env badForm                          = throwError $ BadSpecialForm "Unrecognized special form" badForm
-- remember that `mapM f args` is just shorthand for `sequence $ f <$> args` where `f :: (a -> m b)` and `args :: [a]`, giving us `m [b]`
-- eval value@(Vector _)        = return $ List [quote, value] -- a vector is a scalar type...
-- try to keep up

-- [TODO] a different way of handling function application that permits functions with the same ID with arity 1, 2 (e.g. the usage of `-` as shorthand for `negate` or shorthand for `subtract`)

-- maybe :: b -> (a -> Maybe b) -> a -> b 
-- maybe takes a default value `b`,
--  a function `f` which takes an `a` and produces `Just b` or `Nothing`, 
--  an `a` which it feeds into `f`,
--  and produces the `b` from `Just b` if `f a` succeeds, or the default value `b` if `f a` produces `Nothing`
-- apply :: String -> [LispVal] -> ThrowsError LispVal
-- apply func args = maybe (throwError $ NotFunction "Unrecognized primitive function args" func) -- default value is a `NotFunction` error
--apply func args = case (lookup func primitives) of
--                    (Just f) -> f args
--                    Nothing  -> (throwError $ NotFunction "Unrecognized primitive function args" func)
--                         ($ args)                                                               -- in the context of `maybe`, this is the function which takes the results of the lookup, and applies `args` to the resuls of the lookup on the next line
--                         (lookup func primitives)  -- 
-- I think these are equivalent. The way the above function works is, it uses the partial application of `args` or `($ args)` as the function, applying them as the arguments to whatver function is returned from lookup
-- so, the idiom here is, we can turn any `x` into _a function of 1 argument that takes a function `f` which is applied to `x`_ , e.g. `f x` = `($ x) f`
-- fucking incredible
-- I think the below example is a lot more clear, because i am a nuwub, and I like case statements
apply :: LispVal -> [LispVal] -> IOThrowsError LispVal
apply (PrimitiveFunc func) args                          = liftThrows $ func args
apply (IOFunc func) args                                 = func args
apply (Func (LispFunc params varargs body closure)) args
  | count params /= count args && varargs == Nothing   = 
    throwError $ NumArgs (count params) args
  | otherwise =
    (liftIO $ bindVars closure $ zip params args) >>= bindVarArgs varargs >>= evalBody
    where
      remainingArgs = drop (length params) args -- the rest of the arguments not applied
      count         = toInteger . length -- [a] -> Integer
                                      -- eval env :: LispVal -> IOThrowsError LispVal
      evalBody env  = liftM last $ mapM (eval env) body -- evaluates each expression in the body, and returns the final result
      bindVarArgs arg env = case arg of
        Just argName  -> liftIO $ bindVars env [(argName, List $ remainingArgs)] -- binds varArgs to argname and appends env
        Nothing       -> return env -- return environment without making any change

-- ok i think i get `apply` now

-- let's keep our eye on the ball.
-- type Env = IORef [(String, IORef LispVal)]
-- readIORef :: IORef a -> IO a
-- so, readIORef on an Env has type:
-- readIORef :: IORef [(String, IORef LispVal)] -> IO [(String, IORef LispVal)]
--
-- maybe False (const True) :: Maybe b -> Bool 
-- maybe False (const True) $ Nothing = Nothing
-- maybe False (const True) $ (Just _) = True
--
-- lookup :: Eq a => a -> [(a, b)] -> Maybe b
-- lookup "a string" :: [(String, b)] -> Maybe b
--
-- maybe False (const True) . lookup "key" :: [(String, b)] -> Bool
--
-- return . maybe False (const True) . lookup "key" :: [(String, b)] -> m Bool (in this case IO Bool)

-- isBound :: String -> Env -> IO Bool
-- isBound var = return . maybe False (const True) . lookup var <=< readIORef

isBound :: Env -> String -> IO Bool
isBound envRef var = readIORef envRef >>= return . maybe False (const True) . lookup var

-- remember;
-- type IOThrowsError = ExceptT LispError IO
-- liftIO :: MonadIO m => IO a -> m a
-- MonadIO is vasically a monad in which IO computations may be embedded,
-- "Any monad built by applying a sequence of monad transformers to the IO monad will be an instance of this class"
--
-- liftIO . readIORef :: MonadIO m => IORef a -> m a,
-- in this case, `m` is of type IOThrowsError
--
-- maybe (throwError $ Default "fuck you") :: MonadError LispError m => (a -> m b) -> maybe a -> m b
-- liftIO . readIORef                      :: MonadIO m => IORef a -> m a
--
-- maybe (throwError $ Default "fuck you") (liftIO . readIORef) :: (MonadError LispError m, MonaidIO m) => Maybe (IORef a) -> m a
getVar :: Env -> String -> IOThrowsError LispVal
--getVar :: Env -> String -> ExceptT LispError IO LispVal
getVar envRef var = do env <- liftIO $ readIORef envRef -- env :: [(String, IORef LispVal)]
                       maybe (throwError $ UnboundVar "Attempted to `get` unbound variable" var)
                             (liftIO . readIORef) -- :: IORef a -> IOThrowsError a
                             (lookup var env)     -- :: Maybe (IORef LispVal)
-- throwError is defined for the MonadError typeclass, of whuch ExceptT (IOThrowsError) is an instance

-- going to take a guess at the type of writeIORef
-- writeIORef :: IORef a -> a -> IO ()
-- -- I WAS RIGHT!
-- so in this case, writeIORef :: IORef [(String, IORef LispVal)] -> IORef LispVal -> IO ()
setVar :: Env -> String -> LispVal -> IOThrowsError LispVal
setVar envRef var value = do env <- liftIO $ readIORef envRef -- env :: [(String, IORef LispVal)], the liftIO turns IO a -> m a
                             maybe (throwError $ UnboundVar "Attempted to `set` unbound variable" var)
                                   (liftIO . (flip writeIORef value)) -- 
                                   (lookup var env)  -- Maybe (IORef LispVal)
                             return value

defineVar :: Env -> String -> LispVal -> IOThrowsError LispVal
defineVar envRef var value = do
  isDefined <- liftIO $ isBound envRef var
  if isDefined
     then setVar envRef var value >> return value -- overwrite the old value
     else liftIO $ do -- inside of IO monad here? yes, since `liftIO :: IO a -> m a`
       valueRef <- newIORef value -- bring `value` into the IO monad, unwrap it so that `valueRef :: IORef LispVal`
       env <- readIORef envRef -- env :: [(Sting, IORef LispVal)]
       writeIORef envRef ((var, valueRef) : env) -- cons (var, valueRef) onto env
       return value -- IO LispVal, returned for convenience

-- binds multiple variables to the environment at once 
bindVars :: Env -> [(String, LispVal)] -> IO Env
bindVars envRef  bindings = readIORef envRef >>= extendEnv bindings >>= newIORef where
    -- [(String, LispVal)] -> [(String, IORef LispVal)] -> IO [(String, IORef LispVal)]
  extendEnv bindings env  = liftM (++ env) (mapM addBinding bindings) 
  addBinding (var, value) = do  -- :: (String, LispVal) -> IO (String, IORef LispVal)
    ref <- newIORef value
    return (var, ref) -- IO (String, IORef LispVal)


-- these /must/ check and make sure only identifiers can be used as arguments
-- they must also verify that no duplicate arguments are used
makeFunction :: Maybe String -> Env -> [LispVal] -> [LispVal] -> IOThrowsError LispVal
makeFunction varargs env params body = return . Func $ LispFunc (map show params) varargs body env

makeNormalFunction :: Env -> [LispVal] -> [LispVal] -> IOThrowsError LispVal
makeNormalFunction = makeFunction Nothing

makeVarArgFunction :: LispVal -> Env -> [LispVal] -> [LispVal] -> IOThrowsError LispVal
makeVarArgFunction = makeFunction . Just . show
