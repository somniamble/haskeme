module Main where

import Parser
import Evaluator
import System.Environment
import Data.List
import LispError
import Interaction

-- some ways to do the same things:
-- unwords ["this is a test"]
-- intercalate " " ["this" "is" "a" "test"]
-- concat $ intersperse " " ["this" "is" "a" "test"]

-- main :: IO ()
-- main = someFunc


-- processArgs :: [String] -> String
-- processArgs [] = "<No arguments>"
-- processArgs (x:[]) = x
-- processArgs (x:[y]) = y ++ x
-- processArgs (x:xs) = intercalate x xs
--
-- main :: IO ()
-- main = putStrLn . processArgs =<< getArgs
-- main = do
--  args <- getArgs
--  putStrLn $ processArgs args

main :: IO ()
main = do args <- getArgs 
          case (length args) of 
              0         -> runRepl
              otherwise -> runOne args 
