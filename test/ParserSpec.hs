module ParserSpec (
  parserTests
  ) where

import Test.Tasty
import Test.Tasty.HUnit
import LispVal
import Data.List
import Parser

import Text.ParserCombinators.Parsec

failureString :: String
failureString = "<failed parse>"

failureAtom :: LispVal
failureAtom = Atom failureString

failed :: String -> Bool
failed = (== failureString)

doParse :: Parser LispVal -> String -> LispVal 
doParse parser input = case parse parser "test" input of
                         Left err -> failureAtom
                         Right val -> val

doParseExpression :: String -> LispVal 
doParseExpression = doParse parseExpression

doReadMany :: String -> LispVal
doReadMany input = case parse (skipMany parseInterTokenSpace >> endBy parseExpression parseInterTokenSpace) "test" input of
                     Left err -> failureAtom
                     Right val -> List val

numberParserTests :: TestTree
numberParserTests = testGroup "Number Parser tests" 
  [testCase "parses integer on a string of digits" $
    doParseExpression "123456789" @?= Number 123456789

  , testCase "can parse negative integers" $
    doParseExpression "-1" @?= Number (-1)

  , testCase "parses float succeeds on digits, '.', digits" $
    doParseExpression "123.013" @?= Float 123.013

  , testCase "parses float succeeds on '.', digits" $
    doParseExpression ".013" @?= Float 0.013

  , testCase "can parse negative floats" $
    doParseExpression "-0.1" @?= Float (-0.1)

  , testCase "can parse negative floats of form \"-.<digits>\"" $
    doParseExpression "-.123" @?= Float (-0.123)

  , testCase "fails on digits with a trailing '.'" $
    doParseExpression "456." @?= failureAtom

  , testCase "ignores leading zeros in parse" $
    doParseExpression "0123456789" @?= Number 123456789

  , testCase "ignores trailing zeros in parse" $
    doParseExpression "123.123000000" @?= Float 123.123

  , testCase "can parse binary numbers" $
    doParseExpression "#b1111" @?= Number 15

  , testCase "fails on invalid binary number" $
    doParseExpression "#b1121" @?= failureAtom

  , testCase "can parse Octal numbers" $
    doParseExpression "#o777" @?= Number 511

  , testCase "fails on invalid octal number" $
    doParseExpression "#o888" @?= failureAtom 

  , testCase "can parse Hexidecimal numbers" $
    doParseExpression "#xFF" @?= Number 255

  , testCase "fails on invalid hexidecimal number" $
    doParseExpression "#xGG" @?= failureAtom 
  ]

characterParserTests :: TestTree
characterParserTests = testGroup "Character Parser Tests" $ 
  printableCharTests ++
  [testCase "parses ESC correctly" $
    doParseExpression "#\\altmode" @?= Character '\ESC'
  , testCase "parses backnext correctly" $
    doParseExpression "#\\backnext" @?= Character '\US'
  , testCase "parses backspace correctly" $
    doParseExpression "#\\backspace" @?= Character '\BS'
  , testCase "parses call correctly" $
    doParseExpression "#\\call" @?= Character '\SUB'
  , testCase "parses linefeed correctly" $
    doParseExpression "#\\linefeed" @?= Character '\LF'
  , testCase "parses page correctly" $
    doParseExpression "#\\page" @?= Character '\FF'
  , testCase "parses return correctly" $
    doParseExpression "#\\return" @?= Character '\CR'
  , testCase "parses rubout correctly" $
    doParseExpression "#\\rubout" @?= Character '\DEL'
  , testCase "parses space correctly" $
    doParseExpression "#\\space" @?= Character ' '
  ] where printableCharTests = makeTest <$> "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_abcdefghijklmnopqrstuvwxyz{|}~`"
          makeTest c = testCase ("parses " ++ [c] ++ " correctly") $ (doParseExpression ("#\\" ++ [c])) @?= (Character c)

stringParserTests :: TestTree
stringParserTests = testGroup "String Parser Tests"
  [ testCase "Parses a string with no escape characters correctly" $
      doParseExpression "\"hello world\"" @?= String "hello world"
  , testCase "Parses a string with escaped double-quote" $
      doParseExpression "\"\\\"this is double quoted\\\" this is not\"" @?= String "\"this is double quoted\" this is not"
  , testCase "Parses the empty string" $
    doParseExpression "\"\"" @?= String ""
  , testCase "Parses a string with escape character \\r correctly" $
    doParseExpression "\"\\r\"" @?= String "\r"
  , testCase "Parses a string with escape character \\n correctly" $
    doParseExpression "\"\\n\"" @?= String "\n"
  , testCase "Parses a string with escape character \\t correctly" $
    doParseExpression "\"\\t\"" @?= String "\t"
  , testCase "Parses a string with escape character \\\\ correctly" $
    doParseExpression "\"\\\\\"" @?= String "\\"
  , testCase "fails on string that is not closed" $
    doParseExpression "\"this will fail" @?= failureAtom
  , testCase "fails on sequence of chars followed by a double quote" $
    doParseExpression "@this will fail\"" @?=  failureAtom
  ]

booleanParserTests :: TestTree
booleanParserTests = testGroup "Boolean Parser Tests"
  [ testCase "Parses #f as Bool False" $
    doParseExpression "#f" @?= Bool False
  , testCase "Parses #t as Bool True" $
    doParseExpression "#t" @?= Bool True 
  ]

listParserTests :: TestTree
listParserTests = testGroup "Lisp Parser Tests"
  [ testCase "parses the empty list" $
    doParseExpression "()" @?= List []
  , testCase "parses a list with some elements" $
    doParseExpression "(1 2 3)" @?= List (Number <$> [1, 2, 3])
  , testCase "parses a list with nested list" $
    doParseExpression "(1 (4 5 6) 3)" @?= List [Number 1, List $ Number <$> [4, 5, 6], Number 3]
  , testCase "parses a list with mixed elements" $
    doParseExpression "(1 () #t #f \"Test\" 1.1)" @?= List [Number 1, List [], Bool True, Bool False, String "Test", Float 1.1]
  , testCase "parses a list with a space after the last element" $
    doParseExpression "(1 2 3 )" @?= List [Number 1, Number 2, Number 3]
  , testCase "parses a list with a space before the first element" $
    doParseExpression "( 1 2 3)" @?= List [Number 1, Number 2, Number 3]
  , testCase "parses a dotted list" $
    doParseExpression "(1 2 . 3)" @?= (DottedList [Number 1, Number 2] $ Number 3)
  , testCase "fails on empty dotted list" $
    doParseExpression "( . )" @?= failureAtom
  , testCase "fails on dotted list with no head" $
    doParseExpression "( . 1)" @?= failureAtom
  , testCase "fails on dotted list with no tail" $
    doParseExpression "(1 . )" @?= failureAtom
  , testCase "parses a dotted list with a space before first term" $
    doParseExpression "( 1 2 . 3)" @?= (DottedList [Number 1, Number 2] $ Number 3)
  , testCase "parses a dotted list with a space after last term" $
    doParseExpression "(1 2 . 3 )" @?= (DottedList [Number 1, Number 2] $ Number 3)
  ]

quoteParserTests :: TestTree
quoteParserTests = let quote = Atom "quote" in
                       testGroup "Quoted parser tests"
  [ testCase "parses a quoted empty list" $
    doParseExpression "'()" @?= List [quote, List []]
  , testCase "parses a quoted list with contents" $
    doParseExpression "'(1 2 3)" @?= List [quote, List [Number 1, Number 2, Number 3]]
  , testCase "parses a quoted atom" $
    doParseExpression "'test" @?= List [quote, Atom "test"]
  , testCase "fails if the quoted string is a malformed list" $
    doParseExpression "'(" @?= failureAtom
  ]

identifierParserTests :: TestTree
identifierParserTests = testGroup "Identifier parser tests"
  [ testCase "peculiar identifier `...`" $
    doParseExpression "..." @?= Atom "..."
  , testCase "parses peculiar identifier `+`" $
    doParseExpression "+" @?= Atom "+"
  , testCase "parses peculiar identifier `-`" $
    doParseExpression "-" @?= Atom "-"
  , testCase "parses identifier starting with special initial" $
    doParseExpression "!abc" @?= Atom "!abc"
  , testCase "parses identifier starting with letters" $
    doParseExpression "abc!" @?= Atom "abc!"
  , testCase "parses identifier with special subsequent after initial" $
    doParseExpression "a+-.@" @?= Atom "a+-.@"
  , testCase "parses identifier with digits after initial" $
    doParseExpression "a1234567890" @?= Atom "a1234567890"
  , testCase "fails to parse identifier starting with special subsequent" $
    doParseExpression "+abcde" @?= failureAtom
  , testCase "fails to parse identifier starting with number" $
    doParseExpression "1a" @?= failureAtom
  ] where
    specialInitial     = "!$%&*/:<>?^_~"
    specialSubsequent  = "+-.@" 
    digits             = "0123456789"
    letters            = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

commentParserTests :: TestTree
commentParserTests = testGroup "comment parser tests"
  [ testCase "ignores a comment" $
    doReadMany " ;this is a comment\nabcde\n" @?= (List [Atom "abcde"])
  , testCase "ignores many comments" $
    doReadMany "a;thisisacommnet\nb;anothecomment\nc;yetanother\n" @?=
      (List [Atom "a", Atom "b", Atom "c"])
  ]

parserTests = testGroup "haskeme expression parsing tests" [characterParserTests, numberParserTests, stringParserTests, listParserTests, booleanParserTests, quoteParserTests, identifierParserTests, commentParserTests]
