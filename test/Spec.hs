import Test.Tasty
import Test.Tasty.HUnit

import ParserSpec (parserTests)

-- This is where all of the tests will end up
main = defaultMain $ testGroup "All tests" [parserTests]
